﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegrationSDKCipherRS30.Services.Interfaces;
using Xamarin.Forms;

namespace IntegrationSDKCipherRS30
{
    public partial class MainPage : ContentPage
    {
        IScannerService _scannerService;

        public MainPage()
        {
            InitializeComponent();
            // Retrieve ScannerService from DependencyService
            _scannerService = DependencyService.Get<IScannerService>();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_scannerService != null)
            {
                // Initialize the Reader
                _scannerService.InitBarcodeReader();
                // Subscribe to BarcodeReceived event
                _scannerService.BarcodeReceived += OnScannerServiceBarcodeReceived;
            }
        }

        async void OnScannerServiceBarcodeReceived(object sender, Events.BarcodeEventArgs e)
        {
            await DisplayAlert("Barcode Received", e.BarcodeMessage, "Ok");
            MainLabel.Text = $"Last barcode read : {e.BarcodeMessage}";
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (_scannerService != null)
            {
                // unsubscribe from the barcode service BarcodeReceveid event
                _scannerService.BarcodeReceived -= OnScannerServiceBarcodeReceived;
                // Need to release the handle on the Barcode Reader
                _scannerService.ReleaseBarcodeReader();
            }
        }


    }
}

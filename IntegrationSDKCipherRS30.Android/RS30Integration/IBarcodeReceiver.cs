﻿namespace IntegrationSDKCipherRS30.Droid.RS30Integration
{
    public interface IBarcodeReceiver
    {
        void Result(string result);

        void ReaderConnected();
    }
}

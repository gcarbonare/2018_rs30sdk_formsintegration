﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Android.Content;
using Com.Cipherlab.Barcode;
using Com.Cipherlab.Barcode.Decoder;
using Com.Cipherlab.Barcode.Decoderparams;
using IntegrationSDKCipherRS30.Droid.RS30Integration;
using IntegrationSDKCipherRS30.Events;
using IntegrationSDKCipherRS30.Services.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(RS30ScannerService))]
namespace IntegrationSDKCipherRS30.Droid.RS30Integration
{
    public class RS30ScannerService : IScannerService, IBarcodeReceiver
    {
        private ReaderManager _mReaderManager;
        private IntentFilter _filter;
        private BarcodeReceiver _barcodereceiver;

        #region IScannerService Implementation

        public event EventHandler<BarcodeEventArgs> BarcodeReceived;

        #region InitBarcodeReader

        public void InitBarcodeReader()
        {
            // Instantiate the ReaderManager
            if (_mReaderManager == null)
            {
                _mReaderManager = ReaderManager.InitInstance(Android.App.Application.Context);
                _filter = new IntentFilter();
                // Add which kind of intent we want to be get 
                _filter.AddAction(GeneralString.IntentSOFTTRIGGERDATA);
                _filter.AddAction(GeneralString.IntentPASSTOAPP);
                _filter.AddAction(GeneralString.IntentREADERSERVICECONNECTED);
                _filter.AddAction(GeneralString.IntentSOFTTRIGGERDATA);

                // Create a barcode receiver and set this service as it's callback handler
                _barcodereceiver = new BarcodeReceiver(this);

                // Register the barcode receiver to be the one handling the intents sent by the barcode scanner
                Android.App.Application.Context.RegisterReceiver(_barcodereceiver, _filter);
            }
        }

        #endregion

        #region SetupBarcodeReader

        public void SetupBarcodeReader()
        {
            // We want to disable the keyboard emulation first.
            ToggleKeyboardEmulation(false);
            // and make the EAN13 and QRCode possible
            ToggleEAN13Readability(true);
            ToggleQRCodeReadability(true);
        }

        #endregion

        #region ReadBarcode

        public void ReadBarcode()
        {
            // Start the reading
            StartReading();
        }

        #endregion

        #region ReleaseBarcodeReader

        public void ReleaseBarcodeReader()
        {
            // Releases the barcode recode and the ReaderManager instance
            if (_mReaderManager != null)
            {
                _mReaderManager.Release();
            }
        }

        #endregion

        #region ToggleKeyboardEmulation
        /// <summary>
        /// Turn on/off the keyboard emulation
        /// </summary>
        /// <param name="Enable">Enable the keyboard emulation</param>
        public void ToggleKeyboardEmulation(bool Enable)
        {
            try
            {
                var readerOutputConf = new ReaderOutputConfiguration();
                // set the keyboard Emulation type
                if (Enable)
                {
                    readerOutputConf.EnableKeyboardEmulation = KeyboardEmulationType.InputMethod;
                }
                else
                {
                    readerOutputConf.EnableKeyboardEmulation = KeyboardEmulationType.None;
                }
                // Apply the setting
                var setter = _mReaderManager.Set_ReaderOutputConfiguration(readerOutputConf);

                Debug.Write(setter == ClResult.SErr
                    ? "Setter ReaderOutputConfiguration: ChangeKeyboard => Failed"
                    : "Setter ReaderOutputConfiguration: ChangeKeyboard => " + (Enable ? "Enable" : "Disable") + " => Successful!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        #endregion

        #region ToggleQRCodeReadability
        /// <summary>
        /// Enable or disable QRCode Readability
        /// </summary>
        /// <param name="canRead"></param>
        public void ToggleQRCodeReadability(bool canRead)
        {
            try
            {
                var qr = new QRCode { Enable = canRead ? Enable_State.True : Enable_State.False };

                var setter = _mReaderManager.Set_Symbology(qr);

                Debug.Write(setter == ClResult.SErr
                    ? "Setter UserPreference: CanReadQRCode => Failed"
                    : "Setter UserPreference: CanReadQRCode => " + qr + " => Successful!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        #endregion

        #region ToggleEAN13Readability
        /// <summary>
        /// Enable or disable the EAN13 readability
        /// </summary>
        /// <param name="canRead"></param>
        public void ToggleEAN13Readability(bool canRead)
        {
            try
            {
                var ean13 = new Ean13 { Enable = canRead ? Enable_State.True : Enable_State.False };

                var setter = _mReaderManager.Set_Symbology(ean13);

                Debug.Write(setter == ClResult.SErr
                    ? "Setter UserPreference: CanReadEAN13 => Failed"
                    : "Setter UserPreference: CanReadEAN13 => " + ean13 + " => Successful!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        #endregion

        #region StartReading

        public void StartReading()
        {
            try
            {
                // Start the reading as if you've clicked on the hardware button 
                // (Run inside a Task to not freeze the UIThread if it's been called on)
                Task.Run(() => _mReaderManager.SoftScanTrigger());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        #endregion

        #endregion

        #region IBarcodeReceiver implementation

        public void Result(string result)
        {
            if (BarcodeReceived != null)
            {
                // Fire the BarcodeReceived Event
                this.BarcodeReceived(this, new BarcodeEventArgs(result));
            }
        }

        public void ReaderConnected()
        {
            // Setup the barcode reader when you're connected to it
            SetupBarcodeReader();
        }

        #endregion
    }
}

﻿using System;
using Android.Content;
using Com.Cipherlab.Barcode;

namespace IntegrationSDKCipherRS30.Droid.RS30Integration
{
    [BroadcastReceiver]
    public class BarcodeReceiver : BroadcastReceiver
    {
        // need to be an IBarcodeReceiver Implementation
        private readonly IBarcodeReceiver _callbackHandler;

        public BarcodeReceiver()
        {
        }

        // Gets a IBarcodeReceiver in parameters to handle the callback
        public BarcodeReceiver(IBarcodeReceiver callbackHandler)
        {
            _callbackHandler = callbackHandler;
        }


        public override void OnReceive(Context context, Intent intent)
        {
            // Handles the intent sent by the Scanner
            if (intent.Action == GeneralString.IntentSOFTTRIGGERDATA)
            {
                var data = intent.GetStringExtra(GeneralString.BcReaderData);

                _callbackHandler?.Result(data);
            }

            if (intent.Action == GeneralString.IntentPASSTOAPP)
            {
                var data = intent.GetStringExtra(GeneralString.BcReaderData);
                _callbackHandler?.Result(data);
            }

            if (intent.Action == GeneralString.IntentREADERSERVICECONNECTED)
            {
                _callbackHandler?.ReaderConnected();
            }

        }

    }
}
